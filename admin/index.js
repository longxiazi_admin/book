const Koa = require('koa');
const app = new Koa();
const user = require('./router/user').router;
const book = require('./router/book').router;
const project = require('./router/project').router;
const seed = require('./router/seed').router;
const view = require('./router/view').router;
const index = require('./router/index').router;


// 静态资源
const static = require("koa-static");
const staticPath = './static';
const path = require('path');


const cors = require('koa2-cors');
// 具体参数我们在后面进行解释
app.use(cors({
    origin: function (ctx) {
        return "*"; // 允许来自所有域名请求
    },
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE'],
    allowHeaders: ['Content-Type', 'Authorization', 'Accept'],
}))


// 页面静态资源
app.use(static(
    path.join(__dirname, staticPath)
))

// 挂载路由中间件
app.use(user.routes()).use(user.allowedMethods());
app.use(book.routes()).use(book.allowedMethods());
app.use(project.routes()).use(project.allowedMethods());
app.use(seed.routes()).use(seed.allowedMethods());
app.use(view.routes()).use(view.allowedMethods());
app.use(index.routes()).use(index.allowedMethods());


app.listen(3000);