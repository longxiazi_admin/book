// 博客文章页面
let mongdb = require('../lib/mongdb');
let check = require('../lib/check'); 
const ObjectID = require('mongodb').ObjectID;


// blog article add
var add = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.body.date && ctx.request.body.article && ctx.request.body.author)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.add({
                date:ctx.request.body.date,
                article:ctx.request.body.article,
                author:ctx.request.body.author
            },'article','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '添加文章成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '添加文章失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.add = add;

// blog article del
var del = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(Pid)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.delOne({
                '_id':new ObjectID(ctx.request.body.Pid)
            },'article','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '添加文章成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '添加文章失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.del = del;

// blog article change
var change = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.body.date && ctx.request.body.article && ctx.request.body.author)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.add({
                '_id':new ObjectID(ctx.request.body.Pid)
            },{
                date:ctx.request.body.date,
                article:ctx.request.body.article,
                author:ctx.request.body.author
            },'article','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '修改文章成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '修改文章失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.add = change;


// blog article find
var findOne = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.body.Pid)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.add({
                '_id':new ObjectID(ctx.request.body.Pid)
            },'article','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '查找文章成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '查找文章失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.add = findOne;

// blog article findall list
var findAll = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.body.Pid)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.add({},'article','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '查找文章成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '查找文章失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.add = findAll;

