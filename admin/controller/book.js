const crawler = require('../lib/crawler');
const ObjectID = require('mongodb').ObjectID;
const mongdb = require('../lib/mongdb');


// add book queue
var queue =async function(ctx){
    if (!(ctx.request.body.bookUrl)){
        return ({
        statu: 200,
        msg: '请输入小说路径',
        data: []
        });
    }
    var tag = await new Promise((resolute)=>{
        var t = crawler.pageData(ctx.request.body.bookUrl);
        resolute(t);
    });
    return tag;
}
module.exports.queue = queue; 

// add book section
var section =async function(ctx){
    if (!(ctx.request.body.bookId)){
        return ({
        statu: 200,
        msg: '请输入小说路径',
        data: []
        });
    }
    var tag = await new Promise((resolute)=>{
        var t = crawler.sectionAll(ctx.request.body.bookId);
        resolute(t);
    });
    return tag;
}
module.exports.section = section;

// find book list
var findAll =async function(ctx){
    var tag = await new Promise((resolute)=>{
        var t = mongdb.mongod.find({},'book','book');
        resolute(t);
    });
    // filter 
    tag.data.map((doc)=>{
        delete doc.section 
    });
    if(tag){
        return ({
            statu: 200,
            msg: '查找小说成功！',
            data: [tag]
        });
    }else{
        return ({
            statu: 200,
            msg: '查找小说失败！',
            data: [{token:token}]
        });
    }
}
module.exports.findAll = findAll;


var findBookSection = async function(ctx){
    var tag = await new Promise((resolute)=>{
        var t = mongdb.mongod.find({
            // url:ctx.request.url
            parentID:ctx.request.body.parentID
        },'section','book');
        resolute(t);
    });
    // filter 
    tag.data.map((doc)=>{
        delete doc.text 
    });
    if(tag){
        return ({
            statu: 200,
            msg: '查找文章成功！',
            data: [tag]
        });
    }else{
        return ({
            statu: 200,
            msg: '查找文章失败！',
            data: [{token:token}]
        });
    }
}
module.exports.findBookSection = findBookSection; 

// find section list
var findSection =async function(ctx){
    var tag = await new Promise((resolute)=>{
        var t = mongdb.mongod.find({
            '_id':new ObjectID(ctx.request.body.sectionId)
        },'section','book');
        resolute(t);
    });
    if(tag){
        return ({
            statu: 200,
            msg: '查找文章成功！',
            data: [tag]
        });
    }else{
        return ({
            statu: 200,
            msg: '查找文章失败！',
            data: [{token:token}]
        });
    }
}
module.exports.findSection = findSection; 





