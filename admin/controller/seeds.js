// 登录
var login =async function(param){
    let mongdb = require('../lib/mongdb');
    let check = require('../lib/check');  
    if (!(param.loginname && param.password)){
        return ({
        statu: 200,
        msg: '用户名或密码不能为空',
        data: []
        });
    }
    // 查询数据库
    var user = await new Promise((resolute) => {
        var t = mongdb.mongod.find({ loginname: param.loginname, password: param.password }, 'user', 'book');
        resolute(t);
    });
    if (user.count){
        // 输入的账户，密码正确,生成用户token与时间token 
        return ({
            statu: 200,
            msg: '登陆成功',
            data: [{
                timeToken: check.check.checkTime()
            }]
        });
    }else{
        return ({
            statu: 200,
            msg: '账户或密码不匹配！',
            data: []
        });
    }
}
module.exports.login = login;

// 创建爬虫任务
var queue =async function(param){
   
}
module.exports.queue = queue;