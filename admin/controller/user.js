let mongdb = require('../lib/mongdb');
let check = require('../lib/check'); 
const ObjectID = require('mongodb').ObjectID;


// 登录
var login =async function(ctx){
    if (!(ctx.request.body.loginname && ctx.request.body.password)){
        return ({
        statu: 200,
        msg: '用户名或密码不能为空',
        data: []
        });
    }
    // 查询数据库
    var user = await new Promise((resolute) => {
        var t = mongdb.mongod.find({ loginname: ctx.request.body.loginname, password: ctx.request.body.password }, 'user', 'book');
        resolute(t);
    });
    if (user.count){
        // 输入的账户，密码正确,生成用户token与时间token 
        return ({
            statu: 200,
            msg: '登陆成功',
            data: [{
                timeToken: check.check.checkTime()
            }]
        });
    }else{
        return ({
            statu: 200,
            msg: '账户或密码不匹配！',
            data: []
        });
    }
}
module.exports.login = login;


// user add
var add = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.body.loginname && ctx.request.body.password && ctx.request.body.name)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.add({
                name:ctx.request.body.name,
                password:ctx.request.body.password,
                loginname:ctx.request.body.loginname
            },'user','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '添加用户成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '添加用户失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.add = add;


// user delete
var del = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.Uid)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.delOne({
                '_id':new ObjectID(ctx.request.Uid)
            },'user','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '删除用户成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '删除用户失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.del = del;

// user change
var change = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.Uid && ctx.request.body.loginname && ctx.request.body.password && ctx.request.body.name)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.change({'_id':new ObjectID(ctx.request.Uid)},{
                name:ctx.request.body.name,
                password:ctx.request.body.password,
                loginname:ctx.request.body.loginname
            },'user','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '删除用户成功！',
                data: [{token:token}]
            });
        }else{
            return ({
                statu: 200,
                msg: '删除用户失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.del = del;

// user findAll list
var findAll = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.find({},'user','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '用户查成功！',
                data: [{token:token,list:tag}]
            });
        }else{
            return ({
                statu: 200,
                msg: '用户查询失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.findAll = findAll;

// user findOne
var findOne = async function(ctx){
    var token = check.check.checkTimeOut(ctx.request.body.token);
    if(token){
        // 用户信息
        if(!(ctx.request.Uid)){
            return ({
                statu: 200,
                msg: '缺少参数！',
                data: []
            });
        }
        var tag = await new Promise((resolute)=>{
            var t = mongdb.mongod.find({'_id':new ObjectID(ctx.request.Uid)},'user','book');
            resolute(t);
        });
        if(tag){
            return ({
                statu: 200,
                msg: '用户查成功！',
                data: [{token:token,list:tag}]
            });
        }else{
            return ({
                statu: 200,
                msg: '用户查询失败！',
                data: [{token:token}]
            });
        }
    }else{
        return ({
            statu: 300,
            msg: '用户未登录或登录超时！',
            data: []
        });
    }
}
module.exports.findOne = findOne;


