// blog interface
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const view = require('../controller/view');

// blog article add
router.post('/view/add', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        view.add(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// blog article del
router.post('/view/del', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        view.del(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// blog article change
router.post('/view/change', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        view.change(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// blog article findAll list
router.post('/view/findall', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        view.findAll(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// blog article find one
router.post('/view/findone', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        view.findOne(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

module.exports.router = router;