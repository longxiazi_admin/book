// 用户路由
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const book = require('../controller/book');

// book interface create book crawler
router.post('/book/queue', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        book.queue(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// book interface create book  crawler
router.post('/book/section', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        book.section(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// book interface find book list
router.post('/book/findAll', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        book.findAll(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// book interface find book section list
router.post('/book/findBookSection', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        book.findBookSection(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

// book interface find book section txt
router.post('/book/findSection', koaBody(), async (ctx) => {
    var msg = await new Promise((resolute)=>{
        book.findSection(ctx).then((a)=>{resolute(a);});
    });
    ctx.body=msg;
});

module.exports.router = router;