// 用户路由
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const user = require('../controller/user');

// 登陆
router.post('/user/login', koaBody(), async (ctx) => {
  var msg = await new Promise((resolute)=>{
    user.login(ctx).then((a)=>{resolute(a);});
  });
  ctx.body=msg;
});

// 用户的增加
router.post('/user/add', koaBody(), async (ctx) => {
  var msg = await new Promise((resolute)=>{
    user.add(ctx).then((a)=>{resolute(a);});
  });
  ctx.body=msg;
});

// 用户的删除
router.post('/user/delect', koaBody(), async (ctx) => {
  var msg = await new Promise((resolute)=>{
    user.delect(ctx).then((a)=>{resolute(a);});
  });
  ctx.body=msg;
});

// 用户信息更改
router.post('/user/change', koaBody(), async (ctx) => {
  var msg = await new Promise((resolute)=>{
    user.change(ctx).then((a)=>{resolute(a);});
  });
  ctx.body=msg;
});

// 用户查询
router.post('/user/findall', koaBody(), async (ctx) => {
  var msg = await new Promise((resolute)=>{
    user.findAll(ctx).then((a)=>{resolute(a);});
  });
  ctx.body=msg;
});


router.post('/user/findone', koaBody(), async (ctx) => {
  var msg = await new Promise((resolute)=>{
    user.findOne(ctx).then((a)=>{resolute(a);});
  });
  ctx.body=msg;
});



module.exports.router = router;