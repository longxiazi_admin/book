// koa重写 
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/book";
// 增加
var add = async (myobj, collname, dbname)=>{
    var db = await new Promise((resolve) => {
        MongoClient.connect(url, function(err,db){
            if(err) throw err;
            console.log('数据库已连接');
            resolve(db);
        });
    });
    if (myobj.length>1){
        var msg = await new Promise((resolve)=>{
            var dbo = db.db(dbname);
            dbo.collection(collname).insertMany(myobj, function (err, res) {
                if (err) throw err;
                console.log("文档插入成功");
                resolve(true);
            })
        })
        return await new Promise((resolve) => {
            db.close();
            resolve(true);
        });
    }else{
        var msg = await new Promise((resolve) => {
            var dbo = db.db(dbname);
            dbo.collection(collname).insertOne(myobj, function (err, res) {
                if (err) throw err;
                console.log("文档插入成功");
                resolve(true);
            })
        })
        return await new Promise((resolve) => {
            resolve(true);
            db.close();
        });
    }
}
//删除
var delOne = async (whereStr, collname, dbname) => {
    var db = await new Promise((resolve) => {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            console.log('数据库已连接');
            resolve(db);
        });
    });
    var msg = await new Promise((resolve) => {
        var dbo = db.db(dbname);
        dbo.collection(collname).deleteMany(whereStr, function (err, res) {
            if (err) throw err;
            console.log("文档删除成功");
            resolve(true);
        })
    })
    return await new Promise((resolve) => {
        db.close();
        resolve(true);
    });
}
// 修改
var change = async (whereStr, upStr, collname, dbname) => {
    var db = await new Promise((resolve) => {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            console.log('数据库已连接');
            resolve(db);
        });
    });
    var msg = await new Promise((resolve) => {
        var dbo = db.db(dbname);
        dbo.collection(collname).updateMany(whereStr, { $set: upStr }, function (err, res) {
            if (err) throw err;
            console.log('文档更新成功');
            resolve(true);
        })
    })
    return await new Promise((resolve) => {
        db.close();
        resolve(true);
    });
}
// 查
var find = async (whereStr, collname, dbname,item) => {
    var db = await new Promise((resolve) => {
        MongoClient.connect(url, function (err, db) {
            if (err) throw err;
            console.log('数据库已连接');
            resolve(db);
        });
    });
    var count = await new Promise((resolve) => {
        var dbo = db.db(dbname);
        dbo.collection(collname).find(whereStr).count((err, res) => {
            if (err) throw err;
            resolve(res);
        })    
    })
    var msg = await new Promise((resolve) => {
        var dbo = db.db(dbname);
        dbo.collection(collname).find(whereStr).toArray((err, res) => {
            if (err) throw err;
            console.log('数据查找成功');
            resolve(res);
        })
    })
    return await new Promise((resolve) => {
        db.close();
        resolve({ data: msg, count: count});
    });
}

var mongod = {};
mongod.add = add;
mongod.delOne = delOne;
mongod.find = find;
mongod.change = change;
// 分页查询
module.exports.mongod = mongod;
