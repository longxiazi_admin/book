// token密匙
const jwt = require('jsonwebtoken'); // 用于签发、解析`token`

var check = {};
var secret = 'this is a token';

// 生成token
check.checkTime = (username)=>{
    return jwt.sign({name: username}, secret, {expiresIn:  390});
}
// 校验时间token
check.checkTimeOut = (token)=>{
    var decoded = jwt.verify(token, secret,function(err){
        try {
            if(err) throw err;
        } catch (err) {
            // 重新开始任务
            return false;
        }
        return true;
    });
    if (decoded){
        //延时
        return check.checkTime(decoded.user);
    }else{
        return false;
    }
}

module.exports.check = check;